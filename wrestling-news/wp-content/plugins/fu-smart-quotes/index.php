<?php
/*
Plugin Name: FU Smart Quotes
Plugin URI: https://gist.github.com/4682690
Description: Disable smart quotes
Version: 1.0
Author: Steve Trewick
Author URI: http://www.enigmaticape.com/blog/the-smallest-possible-wordpress-plugin/
License: Public Domain
*/
 
    /* F*** you, smart quotes */
    remove_filter('the_content', 'wptexturize');
 
?>