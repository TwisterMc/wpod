/**
 * $Wordpress TinyMCE configuration $
 * Courtesy of http://mudbomb.com/ $
 */
	tinyMCE.init({
		mode : "exact",
		theme : "advanced",
		language : "en",
		plugins : "searchreplace,ibrowser,emotions",
		theme_advanced_disable : "strikethrough,visualaid,anchor,zoom,table,row_before,row_after,delete_row,separator,col_before,col_after,delete_col,bullist,numlist,outdent,indent,undo,redo,link,unlink,anchor,image,cleanup,help,code,preview,sub,sup,separator,charmap,removeformat,visualaid,hr,formatselect,bold,italic,underline,justifyleft,justifyright,justifycenter,justifyfull,cut,copy,paste,forecolor,backcolor",
		theme_advanced_buttons1_add : "bold,italic,underline,strikethrough,forecolor,backcolor,removeformat,numlist,bullist,outdent,indent,justifyleft,justifyright,justifycenter,justifyfull",
		theme_advanced_buttons2_add : "link,unlink,separator,image,ibrowser,separator,emotions,separator,search,replace,separator,cut,copy,paste,separator,code",
		relative_urls : false,
		remove_script_host : false,
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		extended_valid_elements : "a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name|style],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]",
		elements : "content"
	});