// UK lang variables

tinyMCELang['lang_insert_emotions_title'] = 'Insert emotion';

tinyMCELang['lang_emotions_desc'] = 'Insert emotion';

tinyMCELang['lang_emotions_mellow'] = 'Mellow';
tinyMCELang['lang_emotions_huh'] = 'Surprised';
tinyMCELang['lang_emotions_happy'] = 'Happy';
tinyMCELang['lang_emotions_worried'] = 'Worried';
tinyMCELang['lang_emotions_wink'] = 'Wink';
tinyMCELang['lang_emotions_spiteful'] = 'Spiteful';
tinyMCELang['lang_emotions_laughing'] = 'Laughing';
tinyMCELang['lang_emotions_cool'] = 'Cool';
tinyMCELang['lang_emotions_dreamer'] = 'Dreamer';
tinyMCELang['lang_emotions_sleeping'] = 'Sleeping';
tinyMCELang['lang_emotions_dry'] = 'Dry';
tinyMCELang['lang_emotions_smiling'] = 'Smiling';
tinyMCELang['lang_emotions_wub'] = 'Wub';
tinyMCELang['lang_emotions_mad'] = 'Mad';
tinyMCELang['lang_emotions_sad'] = 'Sad';
tinyMCELang['lang_emotions_unsure'] = 'Unsure';
tinyMCELang['lang_emotions_stunned'] = 'Stunned';
tinyMCELang['lang_emotions_blink'] = 'Blink';
tinyMCELang['lang_emotions_lol'] = 'LOL';
tinyMCELang['lang_emotions_deceived'] = 'Sad';
tinyMCELang['lang_emotions_reproaching'] = 'Reproaching';
tinyMCELang['lang_emotions_annoyed'] = 'Annoyed';
tinyMCELang['lang_emotions_innocent'] = 'Innocent';
tinyMCELang['lang_emotions_down'] = 'Down';
tinyMCELang['lang_emotions_bad'] = 'Bad';
tinyMCELang['lang_emotions_surrender'] = 'Surrender';
tinyMCELang['lang_emotions_furious'] = 'Furious';
tinyMCELang['lang_emotions_applauding'] = 'Applauding';
tinyMCELang['lang_emotions_king'] = 'King';
tinyMCELang['lang_emotions_help'] = 'Help';
tinyMCELang['lang_emotions_flush'] = 'Flush';
tinyMCELang['lang_emotions_bye'] = 'Bye';
tinyMCELang['lang_emotions_wallbashing'] = 'Wallbashing';
tinyMCELang['lang_emotions_thumbup'] = 'Thumbup';
tinyMCELang['lang_emotions_thumbdown'] = 'Thumbdown';
tinyMCELang['lang_emotions_drunk'] = 'Drunk';
tinyMCELang['lang_emotions_smoking'] = 'Smoking';
tinyMCELang['lang_emotions_blush'] = 'Blush';
tinyMCELang['lang_emotions_tongue2'] = 'Tongue';
tinyMCELang['lang_emotions_dunce'] = 'Dunce';
tinyMCELang['lang_emotions_shock'] = 'Shock';
tinyMCELang['lang_emotions_ponder'] = 'Ponder';
tinyMCELang['lang_emotions_doh'] = 'DOH';
tinyMCELang['lang_emotions_jittery'] = 'Jittery';
tinyMCELang['lang_emotions_guns'] = 'Guns';



