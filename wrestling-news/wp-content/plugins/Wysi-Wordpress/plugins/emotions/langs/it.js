//IT lang variables

tinyMCELang['lang_insert_emotions_title'] = 'Inserisci una emoticon';
tinyMCELang['lang_emotions_desc'] = 'Emoticon';

tinyMCELang['lang_emotions_mellow'] = 'Calmo';
tinyMCELang['lang_emotions_huh'] = 'Sorpreso';
tinyMCELang['lang_emotions_happy'] = 'Felice';
tinyMCELang['lang_emotions_worried'] = 'Preccupato';
tinyMCELang['lang_emotions_wink'] = 'Ammiccante';
tinyMCELang['lang_emotions_spiteful'] = 'Dispettoso';
tinyMCELang['lang_emotions_laughing'] = 'Ride forte';
tinyMCELang['lang_emotions_cool'] = 'Duro';
tinyMCELang['lang_emotions_dreamer'] = 'Sognatore';
tinyMCELang['lang_emotions_sleeping'] = 'Addormentato';
tinyMCELang['lang_emotions_dry'] = 'Sospettoso';
tinyMCELang['lang_emotions_smiling'] = 'Sorridente';
tinyMCELang['lang_emotions_wub'] = 'Innamorato';
tinyMCELang['lang_emotions_mad'] = 'Pazzo';
tinyMCELang['lang_emotions_sad'] = 'Triste';
tinyMCELang['lang_emotions_unsure'] = 'Insicuro';
tinyMCELang['lang_emotions_stunned'] = 'Stordito';
tinyMCELang['lang_emotions_blink'] = 'Occhiolino';
tinyMCELang['lang_emotions_lol'] = 'LOL';
tinyMCELang['lang_emotions_deceived'] = 'Deluso';
tinyMCELang['lang_emotions_reproaching'] = 'Rimprovera';
tinyMCELang['lang_emotions_annoyed'] = 'Annoiato';
tinyMCELang['lang_emotions_innocent'] = 'Innocente';
tinyMCELang['lang_emotions_down'] = 'Demoralizzato';
tinyMCELang['lang_emotions_bad'] = 'Cattivo';
tinyMCELang['lang_emotions_surrender'] = 'Arrendevole';
tinyMCELang['lang_emotions_furious'] = 'Furioso';
tinyMCELang['lang_emotions_applauding'] = 'Plaudente';
tinyMCELang['lang_emotions_king'] = 'Il Re';
tinyMCELang['lang_emotions_help'] = 'Cerco aiuto';
tinyMCELang['lang_emotions_flush'] = 'Un cesso';
tinyMCELang['lang_emotions_bye'] = 'Saluta';
tinyMCELang['lang_emotions_wallbashing'] = 'Testa al muro';
tinyMCELang['lang_emotions_thumbup'] = 'Approva';
tinyMCELang['lang_emotions_thumbdown'] = 'Disapprova';
tinyMCELang['lang_emotions_drunk'] = 'Ubriaco';
tinyMCELang['lang_emotions_smoking'] = 'Fumatore';

