<?php
require_once('../../../../../wp-config.php');
get_currentuserinfo();
if ($user_level <= 6) //Checks to see if user has logged in
	die (__("You do not have permission to upload."));
?>
<?php 
// WYSI-Wordpress-- Set the path to the directory for your image uploads below.
// This directory should be RELATIVE to your blog directory.
// For example if your blog is at http://mysite.com/blog and
// your image directory is at http://mysite.com/blog/images/, then
// this setting would be "images/". Note that there is no beginning
// slash, but there IS a trailing slash.
$imagedirectory = "wp-content/images/";


// ================================================
// tinymce PHP WYSIWYG editor control
// ================================================
// Configuration file
// ================================================
// Developed: j-cons.com, mail@j-cons.com
// Copyright: j-cons (c)2004 All rights reserved.
// ------------------------------------------------
//                                   www.j-cons.com
// ================================================
// changed by Josef Klimosz, ok2wo@centrum.cz
// v.1.10, 2005-01-03
// ================================================


// directory where ibrowser files are located
$tinyMCE_dir = 'wp-content/plugins/Wysi-Wordpress/plugins/ibrowser/'; // without preliminary, with trailing slash
// calculation of base document root (useful if safe_mode is on or server does not give the appropriate info)
//get the blog root directory
$root = ABSPATH;
//make sure it ends with a trailing slash
if (!ereg('/$',$root))
  $root = $root.'/';
else
  $root = $root;
 //set the tinyMCE root
$tinyMCE_root = $root.$tinyMCE_dir;
//get the blog url
$blogbase = get_settings('siteurl');
//make sure it ends with a trailing slash
if (!ereg('/$',$blogbase))
  $blogbase = $blogbase.'/';
else
  $blogbase = $blogbase;
//set the tinymce base url
$tinyMCE_base_url = $blogbase;
  
// image library related config

// allowed extentions for uploaded image files
$tinyMCE_valid_imgs = array('gif', 'jpg', 'jpeg', 'png');

// allow upload in image library
$tinyMCE_upload_allowed = true;

// allow delete in image library
$tinyMCE_img_delete_allowed = true;

//examples of image folders setting
$tinyMCE_imglibs=array();

   $tinyMCE_imglibs[]= array(
                             'value'=>"$imagedirectory", //trailing slash
                             'text' =>'Blog Images');


//  $tinyMCE_imglibs[] = array(
//                              'value'   => 'images/', //trailing slash
//                               'text'    => 'Pictures 2');
//                               
//  $tinyMCE_imglibs[] = array(
//                              'value'   => 'gallery/', //trailing slash
//                               'text'    => 'Pictures 3'); 
//
// file to include in img_library.php (useful for setting $tinyMCE_imglibs dynamically
// $tinyMCE_imglib_include = '';
?>
