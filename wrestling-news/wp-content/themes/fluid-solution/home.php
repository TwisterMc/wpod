<?php
/*
Template Name: Home
*/
?>

<?php get_header(); ?>

	<div class="content">
	<?php if (is_home() && !is_paged()) { ?>
	   <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Home Widget Area") ) : ?><?php endif; ?>
	<?php } ?>
	
		<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
		<div class="post" id="post-<?php the_ID(); ?>">
			<?php if ( has_post_thumbnail() ) { ?>
		<div class="postThumbnail">
			<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>">
				<?php the_post_thumbnail('thumbnail'); ?>
			</a>
		</div>
	<?php } ?>
			<h1><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h1>
			<div class="entry">
				<?php the_excerpt('Read the rest of this entry &raquo;'); ?>	  
			</div>
		</div>
		<?php endwhile; ?>
		
		<?php if (is_home() && !is_paged()) { ?>
	       <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Home Widget Bottom") ) : ?><?php endif; ?>
	   <?php } ?>
	   <br clear="both" />
		
        <p align="center"><?php next_posts_link('&laquo; Previous Entries') ?> <?php previous_posts_link('Next Entries &raquo;') ?></p>


	<?php else : ?>

	<h2 align="center">Not Found</h2>

	<p align="center">Sorry, but you are looking for something that isn't here.</p>

	<?php endif; ?>

		
	</div>

</div>
<?php get_sidebar(); ?>

<?php get_footer(); ?>



</body>

</html>