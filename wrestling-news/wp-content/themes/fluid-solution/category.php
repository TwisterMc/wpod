<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

	<div class="content">

	<h1 class="categoryTitle"><?php printf( __( '%s', 'twentyeleven' ), '' . single_cat_title( '', false ) . '' ); ?></h1>
	<?php
        $category_description = category_description();
        if ( ! empty( $category_description )  && !is_paged() )
        echo apply_filters( 'category_archive_meta', '<div class="category-archive-meta">' . $category_description . '</div>' );
        ?>
	
	<?php if (have_posts()) : ?>

	<?php while (have_posts()) : the_post(); ?>

	<div class="post" id="post-<?php the_ID(); ?>">
		
		<?php if ( has_post_thumbnail() ) { ?>
		<div class="postThumbnail">
			<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>">
				<?php the_post_thumbnail('thumbnail'); ?>
			</a>
		</div>
	<?php } ?>

	<h1><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h1>


	<div class="entry">

	<?php the_excerpt(); ?>

	</div>


	</div>


	<?php endwhile; ?>

	<p align="center"><?php next_posts_link('&laquo; Previous Entries') ?> &nbsp;&nbsp;&nbsp;&nbsp; <?php previous_posts_link('Next Entries &raquo;') ?></p>

	<?php else : ?>

	<h2 align="center">Not Found</h2>

	<p align="center">Sorry, but you are looking for something that isn't here.</p>

	<?php endif; ?>

	</div>

</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>

</body>
</html>