<?php
/*
Template Name: Sitemap
*/
?>
<?php get_header(); ?>
<div class="content">
	<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
	<div class="post" id="post-<?php the_ID(); ?>">
    	<h1><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h1>
    	<div class="entry">
        	<?php the_content('Read the rest of this entry &raquo;'); ?>
        	<h2>A Few Categories</h2>
			 <ul><?php wp_list_categories('orderby=name&title_li='); ?></ul>
			 
			 <h2>Top Tags</h2>
			 <?php wp_tag_cloud( 'number=50' ); ?>
			 <br /><br /><br /><br />
			 
        	<?php 				 
			/*
			*  Loop through a Repeater field
			*/
			if( get_field('product') ): ?>
			<div class="product-wrap">
				<?php while( has_sub_field('product') ): ?>
			 
					<div class="product-detail">
					    <h4><?php the_sub_field('title'); ?></h4>
						<a href="<?php the_sub_field('url'); ?>">
						    <?php
						    $attachment_id = get_sub_field('image');
						    $size = "medium"; // (thumbnail, medium, large, full or custom size)
						     
						    $image = wp_get_attachment_image_src( $attachment_id, $size );
						    ?>
						    <img src="<?php echo $image[0]; ?>" alt="<?php the_sub_field('title'); ?>" />
						</a>
					    <p><?php the_sub_field('description'); ?></p>
					</div>
			 
				<?php endwhile; ?>
			 </div>
			<?php endif; ?>
    	</div>
    </div>
    <?php endwhile; ?>
    <p align="center"><?php next_posts_link('&laquo; Previous Entries') ?> <?php previous_posts_link('Next Entries &raquo;') ?></p>
    <?php else : ?>
    <h2 align="center">Not Found</h2>
    <p align="center">Sorry, but you are looking for something that isn't here.</p>
	<?php endif; ?>
	</div>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
</body>
</html>