<?php get_header(); ?>
    <div class="contentPhoto">
    <h1 class="categoryTitle"><?php single_cat_title(); ?></h1>
    <div><?php echo category_description(); ?></div>
    <?php if (have_posts()) : ?>

    <?php while (have_posts()) : the_post(); ?>

    <div class="postPhoto" id="post-<?php the_ID(); ?>">
        <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>">
        <?php if ( has_post_thumbnail() ) {
                echo get_the_post_thumbnail( $page->ID, array( 280, 280) );
            } else { ?>
                <img src="/wrestling-news/wp-content/uploads/2016/02/video-placeholder.jpg" alt="<?php the_title(); ?>" />
        <?php } ?>
        </a>
    </div>
    
    <?php endwhile; ?>
        <div class="clearer">&nbsp;</div>


    <p align="center"><?php next_posts_link('&laquo; Previous Photos') ?> &nbsp;&nbsp;&nbsp; <?php previous_posts_link('Next Photos &raquo;') ?></p>

    <?php else : ?>

    <h2 align="center">Not Found</h2>

    <p align="center">Sorry, but you are looking for something that isn't here.</p>

    <?php endif; ?>
        
    </div>
</div>
<?php get_footer(); ?>

</body>
</html>