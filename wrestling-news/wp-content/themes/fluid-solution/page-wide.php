<?php
/*
Template Name: Wide
*/
?>
<?php get_header(); ?>
<div class="content wide">
<div class="noMobile">
<script type="text/javascript"><!--
google_ad_client = "ca-pub-8716721475579600";
/* WPod Big */
google_ad_slot = "7103747536";
google_ad_width = 728;
google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="//pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
</div>
	<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
	<div class="post" id="post-<?php the_ID(); ?>">
    	<h1><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h1>
    	<div class="entry">
        	<div class="social">
        	    <div style="margin-top:5px;">
        	        <span class='st_sharethis_hcount' displayText='ShareThis'></span>
        	        <span class='st_email_hcount' displayText='Email'></span>
        	    </div>
        	    <div class="fsite" style="margin-top:5px;">
        	        <div class="fb-like" data-href="<?php echo get_permalink(); ?>" data-send="true" data-width="450" data-show-faces="true"></div>
        	    </div>
        	    <div class="twitter">
        	        <a href="//twitter.com/share" class="twitter-share-button" data-url="<?php echo get_permalink(); ?>" data-text="<?php echo the_title('','',false); ?>" data-count="horizontal" data-via="wrestling">Tweet</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
        	    </div>
    	</div>
        	<?php the_content('Read the rest of this entry &raquo;'); ?>
        	<?php 				 
			/*
			*  Loop through a Repeater field
			*/
			if( get_field('product') ): ?>
			<div class="product-wrap">
				<?php while( has_sub_field('product') ): ?>
			 
					<div class="product-detail">
					    <h4><?php the_sub_field('title'); ?></h4>
						<a href="<?php the_sub_field('url'); ?>">
						    <?php
						    $attachment_id = get_sub_field('image');
						    $size = "medium"; // (thumbnail, medium, large, full or custom size)
						     
						    $image = wp_get_attachment_image_src( $attachment_id, $size );
						    ?>
						    <img src="<?php echo $image[0]; ?>" alt="<?php the_sub_field('title'); ?>" />
						</a>
					    <p><?php the_sub_field('description'); ?></p>
					</div>
			 
				<?php endwhile; ?>
			 </div>
			<?php endif; ?>
    	</div>
    </div>
    <?php endwhile; ?>
    <p align="center"><?php next_posts_link('&laquo; Previous Entries') ?> <?php previous_posts_link('Next Entries &raquo;') ?></p>
    <?php else : ?>
    <h2 align="center">Not Found</h2>
    <p align="center">Sorry, but you are looking for something that isn't here.</p>
	<?php endif; ?>
	</div>
</div>
<?php get_footer(); ?>
</body>
</html>