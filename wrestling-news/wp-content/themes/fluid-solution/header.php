<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head>
<meta charset="UTF-8"/>
<title><?php wp_title(''); ?></title>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>?v=1.2" type="text/css" media="screen" />
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
<!-- <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" /> -->

<?php 
if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
		wp_head(); ?>
		
<meta name="viewport" content="initial-scale=1">

</head>

<body <?php body_class(); ?>>

<div class="themat">
<div class="takedown">
    <h1 class="header"><a href="//www.wrestlingpod.com">WrestlingPod</a></h1>
    <h2 class="desc"><?php bloginfo('description'); ?></h2>
    <div class="noMobile">
    	<?php wp_nav_menu( array('menu' => 'Header' )); ?>
    </div>
    <div class="yesMobile">
    	<?php wp_nav_menu( array('menu' => 'Mobile' )); ?>
    </div>
</div>

<div class="">
<?php if (is_home() || is_page(6508) ) { } else { ?>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- WPod Responsive -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-8716721475579600"
     data-ad-slot="8276700739"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
    <br><br>
<?php } ?>
</div>

<div class="left">
