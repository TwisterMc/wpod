<?php get_header(); ?>
	<div class="content">
	<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
	<div class="post" id="post-<?php the_ID(); ?>">
	<h1><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h1>
	<div class="entry">
	<?php the_content(); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>

	</div>
	<br /><br />
	<div class="descr">
	<?php if ( get_post_meta($post->ID, 'storylink', true) ) : ?>
	    <a href="<?php echo get_post_meta($post->ID, 'storylink', true) ?>" target="_blank">Source</a> | 
	<?php endif; ?>
	Posted <?php the_time('F jS, Y') ?>. Filed under <?php the_category(', ') ?>  <?php the_tags( 'Tagged: ', ', ', ''); ?> <?php edit_post_link('Edit','','<strong>  </strong>'); ?></div>
	</div>

<div class="">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- WPod Responsive -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-8716721475579600"
     data-ad-slot="8276700739"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
		<?php comments_template(); ?>
	<?php endwhile; ?>
	<?php 				 
	
	//*  Loop through a Repeater field
	
	
	    $value = get_field( "product", 3667 ); 
	    
	    $i = 0;
	    while ($i <= 5) {
	    $i++;
	    
	?>
	<div class="product-wrap">
	 
		<div class="product-detail alt">
		    <h4><?php echo $value[$i]['title']; ?></h4>
			<a href="<?php echo $value[$i]['url']; ?>">
			    <?php
			    $attachment_id = $value[$i]['image'];
			    $size = "thumbnail"; // (thumbnail, medium, large, full or custom size)
			     
			    $image = wp_get_attachment_image_src( $attachment_id, $size );
			    ?>
			    <img src="<?php echo $image[0]; ?>" alt="<?php echo $value[$i]['title']; ?>" />
			</a>
		</div>
	 
	 </div>
	<?php } ?>
	
	<p align="center"><?php next_posts_link('&laquo; Previous Entries') ?> <?php previous_posts_link('Next Entries &raquo;') ?></p>
	<?php else : ?>
	<h2 align="center">Not Found</h2>
	<p align="center">Sorry, but you are looking for something that isn't here.</p>
	<?php endif; ?>
	</div>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
</body>
</html>